# README #

Test form with user data and google map integration.

User inputs his data and ticks location on the google map.

On submit app validates inputs and post JSON data on success case

### Main parts of the app ###

App is built with assistance of React/Redux.

It is a single page application with two routes:

* Home - for the app itself located on the root
* Not Found - default screen for other locations

Before submit you have to tick location on the map.

My location button will open it

App has client side validation for fields before submitting.

If validation is passed app sends JSON to /submit API which is just node express controller

If POST data is successful app shows the message in the top

### How do build the app ###

* Clone this repo
* npm install
* npm start
* open localhost:3000 to see the app in action
