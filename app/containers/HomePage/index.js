import React from 'react';
import { connect } from 'react-redux';
import Input from "../../components/Input";
import Button from "../../components/Button";
import GoogleMapComponent from "../../components/GoogleMap";

import {submit} from "../App/actions";
import "./style.less";

// array with rules for input elements
const formElements = [
    {
        name: "username",
        placeholder: "Username",
        required: true,
        errorText: "Username length must be more than 6 chars",
        validate: (value) => {
            return value.length >= 6;
        }
    },
    {
        name: "mail",
        placeholder: "E-mail",
        required: true,
        errorText: "Not valid E-Mail",
        validate: (value) => {
            return value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        }
    },
    {
        name: "phone",
        placeholder: "Phone",
        required: false,
        errorText: "Not valid phone",
        validate: (value) => {
            return /^\d+$/.test(value);
        }
    }
];

class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
		super(props);
        this.state = {
            isGoogleMapShown: false,
            errors: {},
            lat: null,
            lng: null
        };
        // setting default input values with empty
        formElements.forEach((element)=>{
            this.state[element.name] = '';
        });
	}
    handleInputChange = (event, name) => {
        // on change input field
		const {value} = event.target;
		this.setState({[name]: value});
	};
    onMapClick = (event) => {
        // on clicking on somewhere on the map
        // saving coordinates
        const {lat, lng} = event.latLng;
        alert(`My location is ${lat()} ${lng()}`);
        this.setState({lat: lat(), lng: lng()});
    };
    submitForm = () => {
        let errors = {};
        let submitData = {};
        // validating input fields with internal rules
        formElements.forEach((element) => {
            submitData[element.name] = this.state[element.name];
            if (!this.state[element.name] && element.required) {
                errors[element.name] = `${element.placeholder} field is required`;
            } else if (!element.validate(this.state[element.name])) {
                errors[element.name] = element.errorText;
            }
        });
        // cheking if there are any coordinates
        if (!this.state.lat && !this.state.lng) {
            errors["google"] = "Location is not checked";
        }
        // update errors
        this.setState({errors});
        if (Object.keys(errors).length === 0) {
            // if there are not amy errors call submit acion with all user valid data
            submitData.lat = this.state.lat;
            submitData.lng = this.state.lng;
            this.props.dispatch(submit(submitData));
            return;
        }
    };
    render() {
        const {status} = this.props;
        return (
            <div>
                {status == "ok" && !Object.values(this.state.errors).length && <h1>Data successfull sent</h1>}
                {
                    formElements.map((item) => {
                        return (
                            <Input
                                type="text"
                                name={item.name}
                                className={`${this.state.errors[item.name] ? "error" : ""}`}
                                placeholder={item.placeholder}
                                value={this.state[item.name]}
                                onChange={(event) => { this.handleInputChange(event, item.name) }}
                                key={item.name}
                            >
                            {item.required ? <span className="required">*</span> : ''}
                            </Input>
                        )
                    })

                }
                {Object.values(this.state.errors).join()}
                <br />
                <Button onClick={()=>{ this.setState({isGoogleMapShown: !this.state.isGoogleMapShown })}}>
                    <span>My location</span>
                </Button>
                <Button onClick={this.submitForm}>
                    <span>Submit</span>
                </Button>
                {this.state.isGoogleMapShown && <GoogleMapComponent onClick={this.onMapClick} />}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
	return {
        status: state.global.status
	};
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
