import {DATA_REQUEST, DATA_SUCCESS, DATA_FAILURE} from "./actions"

const initialState = {
};

function appReducer(state = initialState, action) {
  switch (action.type) {
      case DATA_REQUEST:
          return {
            ...state,
            isFetching: true
          }
      case DATA_SUCCESS:
          return {
            ...state,
            isFetching: false,
            status: "ok"
          }
      case DATA_FAILURE:
          return {
            ...state,
            isFetching: false
          }
      default:
        return state;
  }
}

export default appReducer;
