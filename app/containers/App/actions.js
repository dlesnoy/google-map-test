export const DATA_REQUEST = "DATA_REQUEST";
export const DATA_SUCCESS = "DATA_SUCCESS";
export const DATA_FAILURE = "DATA_FAILURE";

function requestData() {
	return {
		type: DATA_REQUEST
	};
}

function receiveData() {
	return {
		type: DATA_SUCCESS
	};
}

function dataError(error) {
	return {
		type: DATA_FAILURE
	};
}

export function submit(data) {
	return (dispatch, getState) => {
		dispatch(requestData());

		return fetch("/submit", {
		  method: 'post',
		  headers: {
		    'Accept': 'application/json, text/plain, */*',
		    'Content-Type': 'application/json'
		  },
		  body: JSON.stringify(data)
		})
			.then((response) => {
				console.log(response)
                if (response.status == 200) {
                    dispatch(receiveData());
                } else {
                    dispatch(dataError());
                }
			})
			.catch((err) => {
				dispatch(dataError());
			});
	};
}
