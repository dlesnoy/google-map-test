import React from "react";
import PropTypes from "prop-types";
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.27&libraries=places,geometry&key=AIzaSyCPw4ufI4M6djAbTbPKuNIHCVNatkaZol0";

const GettingStartedGoogleMap = withGoogleMap(props => (
    <GoogleMap
      defaultZoom={3}
      defaultCenter={{ lat: -25.363882, lng: 131.044922 }}
      onClick={props.onClick}
    >
    </GoogleMap>
));

const GoogleMapComponent = (props) => {
	const {onClick} = props;
	return (
		<div style={{ position: `absolute`}}>
			<GettingStartedGoogleMap
				onClick={onClick}
				googleMapURL={googleMapURL}
				containerElement={
				  <div style={{ height: `600px`, width: `600px`}} />
				}
				mapElement={
				  <div style={{height: `600px`, width: `600px` }} />
				}
			/>
		</div>
	);
}

const propTypes = {
	onClick: PropTypes.func
};

GoogleMapComponent.propTypes = propTypes;
export default GoogleMapComponent;
