import React from "react";
import PropTypes from "prop-types";

const propTypes = {
	onClick: PropTypes.func
};

const Button = (props) => {
	const {onClick, children} = props;

	return (
		<button
			onClick={onClick}
		>
			{children}
		</button>
	);
};

Button.propTypes = propTypes;
export default Button;
