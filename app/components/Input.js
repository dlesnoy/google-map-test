import React from "react";
import PropTypes from "prop-types";

const Input = (props) => {
	const {type, name, className, placeholder, value, onChange, children} = props;
	return (
		<div>
			{children}
			<input
				className={className}
				type={type}
				name={name}
				placeholder={placeholder}
				value={value}
				onChange={onChange}
			/>
		</div>
	);
}

Input.propTypes = {
	type: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	className: PropTypes.string,
	placeholder: PropTypes.string.isRequired,
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired
};

export default Input;
